#プロジェクト概要
このソフトウエアは、[railcar](https://github.com/oracle/railcar/blob/master/README.md) と [mono](https://github.com/mono/mono/blob/main/) のフォークです。 C# のみの DI コンテナーとして高速に実行され、OS として機能するように処理しようとしています。
